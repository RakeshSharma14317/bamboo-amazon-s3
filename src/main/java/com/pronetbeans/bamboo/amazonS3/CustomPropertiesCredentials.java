package com.pronetbeans.bamboo.amazonS3;

import com.amazonaws.auth.AWSCredentials;

/**
 * A bean holding the AWS Access Key and Secret Key.
 *
 * @author Adam Myatt
 */
public class CustomPropertiesCredentials implements AWSCredentials {

    private String accessKey = null;
    private String secretAccessKey = null;

    /**
     * @param accessKey the accessKey to set
     */
    public void setAWSAccessKeyId(String accessKey) {
        this.accessKey = accessKey;
    }

    /**
     * @param secretAccessKey the secretAccessKey to set
     */
    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }

    @Override
    public String getAWSAccessKeyId() {
        return accessKey;
    }

    @Override
    public String getAWSSecretKey() {
        return secretAccessKey;
    }
}
