package com.pronetbeans.bamboo.amazonS3;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Class for configuring parameters used by class for downloading file from
 * Amazon S3 bucket.
 *
 * @author Adam Myatt
 */
public class DownloadFileFromAmazonS3Configurator extends AbstractTaskConfigurator {

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("propsBucketName", params.getString("propsBucketName"));
        config.put("propsFileToDownload", params.getString("propsFileToDownload"));
        config.put("propsLocalFileName", params.getString("propsLocalFileName"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.put("propsBucketName", taskDefinition.getConfiguration().get("propsBucketName"));
        context.put("propsFileToDownload", taskDefinition.getConfiguration().get("propsFileToDownload"));
        context.put("propsLocalFileName", taskDefinition.getConfiguration().get("propsLocalFileName"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        context.put("propsBucketName", taskDefinition.getConfiguration().get("propsBucketName"));
        context.put("propsFileToDownload", taskDefinition.getConfiguration().get("propsFileToDownload"));
        context.put("propsLocalFileName", taskDefinition.getConfiguration().get("propsLocalFileName"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        final String propsBucketName = params.getString("propsBucketName");
        if (StringUtils.isEmpty(propsBucketName)) {
            errorCollection.addError("propsBucketName", textProvider.getText("com.pronetbeans.bamboo.amazonS3.DownloadFileFromAmazonS3Task.error"));
        }
        final String propsFileToDownload = params.getString("propsFileToDownload");
        if (StringUtils.isEmpty(propsFileToDownload)) {
            errorCollection.addError("propsFileToDownload", textProvider.getText("com.pronetbeans.bamboo.amazonS3.DownloadFileFromAmazonS3Task.error"));
        }
        final String propsLocalFileName = params.getString("propsLocalFileName");
        if (StringUtils.isEmpty(propsLocalFileName)) {
            errorCollection.addError("propsLocalFileName", textProvider.getText("com.pronetbeans.bamboo.amazonS3.DownloadFileFromAmazonS3Task.error"));
        }
    }

    public void setTextProvider(final TextProvider textProvider) {
        this.textProvider = textProvider;
    }
}
