package com.pronetbeans.bamboo.amazonS3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bamboo.task.*;
import java.util.Map;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Bamboo Task for copying a file between S3 Buckets.
 *
 * @author Adam Myatt
 */
public class CopyFileToAmazonS3Task implements TaskType {

    private static final Logger log = Logger.getLogger(CopyFileToAmazonS3Task.class);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final BuildLogger buildLogger = taskContext.getBuildLogger();

        Map<String, String> custom = taskContext.getBuildContext().getParentBuildContext().getBuildDefinition().getCustomConfiguration();

        StringEncrypter encrypter = new StringEncrypter();

        String encryptedAccessKey = custom.get(Constants.ACCESS_KEY);
        String encryptedSecretKey = custom.get(Constants.SECRET_KEY);

        String propsAccessKey = null;
        String propsSecretKey = null;

        try {
            propsAccessKey = encrypter.decrypt(encryptedAccessKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            propsSecretKey = encrypter.decrypt(encryptedSecretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String propsSourceBucket = taskContext.getConfigurationMap().get("propsSourceBucket");
        final String propsSourceFile = taskContext.getConfigurationMap().get("propsSourceFile");
        final String propsDestBucket = taskContext.getConfigurationMap().get("propsDestBucket");
        final String propsDestFile = taskContext.getConfigurationMap().get("propsDestFile");

        boolean itWorked = false;

        try {
            CustomPropertiesCredentials cpc = new CustomPropertiesCredentials();
            cpc.setAWSAccessKeyId(propsAccessKey);
            cpc.setSecretAccessKey(propsSecretKey);
            
            final AmazonS3 s3 = new AmazonS3Client(cpc);

            CopyObjectResult cor = s3.copyObject(propsSourceBucket, propsSourceFile, propsDestBucket, propsDestFile);

            buildLogger.addBuildLogEntry("Copied File " + propsSourceFile + " from Bucket " + propsSourceBucket + " to Bucket " + propsDestBucket);

            itWorked = true;

            log.info(Utils.getLogBanner());

        } catch (Exception e) {
            buildLogger.addBuildLogEntry("Error copying file between Amazon S3 Buckets : " + e.getMessage());
            e.printStackTrace();
        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }
}