package com.pronetbeans.bamboo.amazonS3;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BaseBuildConfigurationAwarePlugin;
import com.atlassian.bamboo.v2.build.configuration.MiscellaneousBuildConfigurationPlugin;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.util.concurrent.LazyReference;
import java.util.Map;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Class for viewing and setting Amazon credential properties.
 *
 * @author Adam Myatt
 */
public class AmazonBuildPlanConfigurationPlan extends BaseBuildConfigurationAwarePlugin implements MiscellaneousBuildConfigurationPlugin {

    private static final Logger log = Logger.getLogger(AmazonBuildPlanConfigurationPlan.class);

//    @Override
//    public String getEditHtml(BuildConfiguration buildConfiguration, Plan plan) {
//        
//        
//        return super.getEditHtml(buildConfiguration, plan);
//    }
    @Override
    public boolean isApplicableTo(@NotNull final Plan plan) {
        return plan instanceof TopLevelPlan;
    }

    @Override
    protected void populateContextForEdit(Map<String, Object> context, BuildConfiguration buildConfiguration, Plan plan) {

        final LazyReference<StringEncrypter> encrypterRef = new LazyReference<StringEncrypter>() {

            @Override
            protected StringEncrypter create() throws Exception {
                return new StringEncrypter();
            }
        };

        String accessKeyValue = isPluginEnabledForPlan(plan.getBuildDefinition(), Constants.ACCESS_KEY, "");
        String decryptedAccessKey = encrypterRef.get().decrypt(accessKeyValue);
        context.put("temporary.accesskey_plain", decryptedAccessKey);
        buildConfiguration.setProperty("temporary.accesskey_plain", decryptedAccessKey);

        String secretKeyValue = isPluginEnabledForPlan(plan.getBuildDefinition(), Constants.SECRET_KEY, "");
        String decryptedSecretKey = encrypterRef.get().decrypt(secretKeyValue);
        context.put("temporary.secretkey_plain", decryptedSecretKey);
        buildConfiguration.setProperty("temporary.secretkey_plain", decryptedSecretKey);
    }

    public static String isPluginEnabledForPlan(@NotNull final BuildDefinition buildDefinition, String key, String defaultValue) {
        final Map<String, String> customConfiguration = buildDefinition.getCustomConfiguration();
        String returnValue = "";
        if (customConfiguration != null && customConfiguration.containsKey(key)) {
            returnValue = customConfiguration.get(key);
        } else {
            if (customConfiguration != null) {
                returnValue = defaultValue;
                customConfiguration.put(key, defaultValue);
                buildDefinition.setCustomConfiguration(customConfiguration);
            }
        }
        return returnValue;
    }

    @Override
    public void prepareConfigObject(@NotNull BuildConfiguration buildConfiguration) {

        final LazyReference<StringEncrypter> encrypterRef = new LazyReference<StringEncrypter>() {

            @Override
            protected StringEncrypter create() throws Exception {
                return new StringEncrypter();
            }
        };

        String accessKeyEncrypted = encrypterRef.get().encrypt(buildConfiguration.getString("temporary.accesskey_plain", "").trim());
        String secretKeyEncrypted = encrypterRef.get().encrypt(buildConfiguration.getString("temporary.secretkey_plain", "").trim());

        if (buildConfiguration.getProperty(Constants.ACCESS_KEY) == null) {
            buildConfiguration.setProperty(Constants.ACCESS_KEY, "");
        } else {
            buildConfiguration.setProperty("temporary.accesskey_plain", accessKeyEncrypted);
            buildConfiguration.setProperty(Constants.ACCESS_KEY, accessKeyEncrypted);
        }

        if (buildConfiguration.getProperty(Constants.SECRET_KEY) == null) {
            buildConfiguration.setProperty(Constants.SECRET_KEY, "");
        } else {
            buildConfiguration.setProperty("temporary.secretkey_plain", secretKeyEncrypted);
            buildConfiguration.setProperty(Constants.SECRET_KEY, secretKeyEncrypted);
        }
    }

    @Override
    public ErrorCollection validate(BuildConfiguration buildConfiguration) {
        return super.validate(buildConfiguration);
    }

    @Override
    public boolean isConfigurationMissing(@NotNull BuildConfiguration buildConfiguration) {
        return buildConfiguration.getProperty(Constants.ACCESS_KEY) == null
                || buildConfiguration.getProperty(Constants.SECRET_KEY) == null;
    }
}
