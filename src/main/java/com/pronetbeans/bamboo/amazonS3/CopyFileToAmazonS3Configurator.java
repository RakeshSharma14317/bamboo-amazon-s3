package com.pronetbeans.bamboo.amazonS3;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Class for configuring parameters used by class for copying files between S3 buckets.
 *
 * @author Adam Myatt
 */
public class CopyFileToAmazonS3Configurator extends AbstractTaskConfigurator {

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("propsSourceBucket", params.getString("propsSourceBucket"));
        config.put("propsSourceFile", params.getString("propsSourceFile"));
        config.put("propsDestBucket", params.getString("propsDestBucket"));
        config.put("propsDestFile", params.getString("propsDestFile"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.put("propsSourceBucket", taskDefinition.getConfiguration().get("propsSourceBucket"));
        context.put("propsSourceFile", taskDefinition.getConfiguration().get("propsSourceFile"));
        context.put("propsDestBucket", taskDefinition.getConfiguration().get("propsDestBucket"));
        context.put("propsDestFile", taskDefinition.getConfiguration().get("propsDestFile"));

    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        context.put("propsSourceBucket", taskDefinition.getConfiguration().get("propsSourceBucket"));
        context.put("propsSourceFile", taskDefinition.getConfiguration().get("propsSourceFile"));
        context.put("propsDestBucket", taskDefinition.getConfiguration().get("propsDestBucket"));
        context.put("propsDestFile", taskDefinition.getConfiguration().get("propsDestFile"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        final String propsSourceBucket = params.getString("propsSourceBucket");
        if (StringUtils.isEmpty(propsSourceBucket)) {
            errorCollection.addError("propsSourceBucket", textProvider.getText("com.pronetbeans.bamboo.amazonS3.CopyFileToAmazonS3Task.error"));
        }
        final String propsSourceFile = params.getString("propsSourceFile");
        if (StringUtils.isEmpty(propsSourceFile)) {
            errorCollection.addError("propsSourceFile", textProvider.getText("com.pronetbeans.bamboo.amazonS3.CopyFileToAmazonS3Task.error"));
        }
        final String propsDestBucket = params.getString("propsDestBucket");
        if (StringUtils.isEmpty(propsDestBucket)) {
            errorCollection.addError("propsDestBucket", textProvider.getText("com.pronetbeans.bamboo.amazonS3.CopyFileToAmazonS3Task.error"));
        }
    }

    public void setTextProvider(final TextProvider textProvider) {
        this.textProvider = textProvider;
    }
}
