[@ww.textfield labelKey="Bucket Name" name="propsBucketName" required='true'/]
[@ww.textfield labelKey="File to Upload" name="propsFileToUpload" required='true'/]
[@ww.checkbox labelKey="Create Bucket If It Doesn't Exist" name="propsCreateBucketIfNotExist" required='false' toggle='true'/]
[@ww.checkbox labelKey="Server-Side Encryption (AES256)" name="propsEncryptFile" required='false' toggle='true'/]