[@ui.bambooSection title='Amazon Web Services Credentials' description='Enter the Access Key and Secret Key for Use By Amazon Tasks' ]
    [@ww.textfield labelKey="AWS Access Key" name="temporary.accesskey_plain" value="${buildConfiguration.getString('temporary.accesskey_plain')}" required='false' /]
    [@ww.textfield labelKey="AWS Secret Key" name="temporary.secretkey_plain" value="${buildConfiguration.getString('temporary.secretkey_plain')}" required='false' /]
[/@ui.bambooSection]

