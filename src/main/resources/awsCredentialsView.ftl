[@ww.label labelKey='AWS Access Key'
    value='${.vars["temporary.accesskey_plain"]?string.substring(0,2)}******' /]
[@ww.label labelKey='AWS Secret Key'
    value='${.vars["temporary.secretkey_plain"]?string.substring(0,2)}******' /]
